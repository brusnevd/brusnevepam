let infoBlock = document.getElementById("task-info"),
    taskText = "";

for (let item of document.getElementsByClassName("item")) {
    item.addEventListener("click", (event) => {
        for (let item1 of document.getElementsByClassName("item")) {
            item1.classList.remove("active");
        }
        item.classList.add("active");
        jsConsole.clear();
        switch (item.innerText) {
            case "Task 1":
                taskText = 'Напишите функцию для извлечения всех адресов электронной почты из данного текста';
                infoBlock.innerText = taskText;
                Task1();
                break;
            case "Task 2":
                taskText = 'Написать программу, которая извлекает из заданного текста все палиндромы, например, "ABBA", "lamal" "exe".';
                infoBlock.innerText = taskText;
                Task2();
                break;
        }
    });
}

function Task1() {
    let input1 = document.createElement("input");
    let span1 = document.createElement("span");
    let span2 = document.createElement("span");
    span1.innerText = "Text - ";
    span2.innerText = "Тестовый текст - asdasd<dima>@<mail>...<com>sadsawdasd<brusnev>@<inbox>...<ru>sadsa";

    let btn = document.createElement("button");
    btn.innerText = "Parse emails";
    let p1 = document.createElement("p");

    p1.appendChild(span1);
    p1.appendChild(input1);
    p1.appendChild(span2);

    infoBlock.appendChild(p1);

    infoBlock.appendChild(btn);

    btn.addEventListener("click", () => {
        let answer = parseMails(input1.value);

        answer.forEach(element => {
            jsConsole.writeLine(element);
        });
    });
}

function Task2() {
    let input1 = document.createElement("input");
    let span1 = document.createElement("span");
    span1.innerText = "Text  - ";
    let span2 = document.createElement("span");
    span2.innerText = "Тестовый текст  - abbbbbbbbbba AbbABbabba sdfffs asaasa";

    let btn = document.createElement("button");
    btn.innerText = "Parse";
    let p1 = document.createElement("p");

    p1.appendChild(span1);
    p1.appendChild(input1);
    p1.appendChild(span2);

    infoBlock.appendChild(p1);

    infoBlock.appendChild(btn);

    btn.addEventListener("click", () => {
        let answer = parsePalindroms(input1.value);

        answer.forEach(element => {
            jsConsole.writeLine(element);
        });
    });
}