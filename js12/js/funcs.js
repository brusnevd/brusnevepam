function parseMails(text) {
    let reg = new RegExp("(<[^>]*>@<[^>]*>\.{3}<[^>]*>)", "ig");
    let res = text.match(reg);
    let reg2 = new RegExp("<([^>]*)>", "ig");
    if (res) {
        res = res.map(cur => {
            let res2 = cur.match(reg2);
            return res2[0].slice(1, res2[0].length - 1) + "@" + res2[1].slice(1, res2[1].length - 1) + "." + res2[2].slice(1, res2[2].length - 1);
        });
        return res;
    }
}

// asdasd<dima>@<mail>.<com>sadsa

function parsePalindroms(text) {
    let mas = text.split(" ");
    mas = mas.filter((item) => item.toLowerCase() === item.split('').reverse().join('').toLowerCase());
    return mas;
}