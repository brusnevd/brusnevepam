let infoBlock = document.getElementById("task-info"),
    taskText = "";

for (let item of document.getElementsByClassName("item")) {
    item.addEventListener("click", (event) => {
        for (let item1 of document.getElementsByClassName("item")) {
            item1.classList.remove("active");
        }
        item.classList.add("active");
        jsConsole.clear();
        switch (item.innerText) {
            case "Task 1":
                taskText = 'Напишите сценарий, который выбирает все узлы <div>, которые непосредственно внутри других элементов <div>';
                infoBlock.innerText = taskText;
                Task1();
                break;
            case "Task 2":
                taskText = `Создайте функцию, которая получает значение <input type="text"> и выводит его значение на консоль`;
                infoBlock.innerText = taskText;
                Task2();
                break;
            case "Task 3":
                taskText = `Создайте функцию, которая получает значение <input type="color"> и задает фон <body> этим значением.`;
                infoBlock.innerText = taskText;
                Task3();
                break;
            case "Task 4":
                taskText = `Напишите скрипт, который создает 5 "div" элементов и перемещает их по круговому пути с ин-тервалом 100 миллисекунд`;
                infoBlock.innerText = taskText;
                Task4();
                break;
        }
    });
}

function Task1() {
    let tagTask1 = document.querySelector("#console>p");
    let htmlTask1 = "<div class='task1divs'><div>Lonely div</div><div><div>Div in div</div><div>Div in div</div></div><div>Another lonely div</div></div>";
    tagTask1.innerHTML = htmlTask1;

    let btn = document.createElement("button");
    btn.innerText = "With querySelectorAll";

    let btn2 = document.createElement("button");
    btn2.innerText = "With getElementsByTagName";

    let p1 = document.createElement("p");

    p1.appendChild(btn);
    p1.appendChild(btn2);
    infoBlock.appendChild(p1);

    btn.addEventListener("click", () => {
        let mas = Array.from(tagTask1.querySelectorAll(".task1divs div>div"));

        mas.forEach((elem) => {
            elem.style.background = "rgba(208,255,0, .5)";
        });

        jsConsole.writeLine("Найдено элементов - " + mas.length);
    });

    btn2.addEventListener("click", () => {
        let block = tagTask1.querySelector(".task1divs");
        let mas = Array.from(block.getElementsByTagName("div"));

        let count = 0;

        mas.forEach((elem) => {
            console.log(elem.parentNode.tagName)
            if (elem.parentNode.tagName == "DIV" && !elem.parentNode.classList.contains("task1divs")) {
                elem.style.background = "rgba(255,0,174, .5)";
                count++;
            }
        });

        jsConsole.writeLine("Найдено элементов - " + count);
    });
}

function Task2() {
    let input1 = document.createElement("input");

    let btn = document.createElement("button");
    btn.innerText = "Console";

    infoBlock.appendChild(input1);
    infoBlock.appendChild(btn);

    btn.addEventListener("click", () => {
        console.log(input1.value)
    });
}

function Task3() {
    let input1 = document.createElement("input");
    input1.type = "color";

    let btn = document.createElement("button");
    btn.innerText = "Color";

    infoBlock.appendChild(input1);
    infoBlock.appendChild(btn);

    btn.addEventListener("click", () => {
        document.body.style.background = input1.value;
    });
}

function Task4() {
    let input1 = document.createElement("input");
    let span = document.createElement("span");
    span.innerText = "Количество точек ";

    let input2 = document.createElement("input");
    let spa2 = document.createElement("span");
    spa2.innerText = "Скорость (от 10 до 200 чем меньше число тем больше скорость) ";

    let input3 = document.createElement("input");
    let span3 = document.createElement("span");
    span3.innerText = "Радиус (от 10 до 200) ";

    let btn = document.createElement("button");
    btn.innerText = "Создать кружки";

    let btn2 = document.createElement("button");
    btn2.innerText = "Анимировать";

    let btn3 = document.createElement("button");
    btn3.innerText = "Очистить поле";

    let p1 = document.createElement("p");
    let p2 = document.createElement("p");
    let p3 = document.createElement("p");
    let p4 = document.createElement("p");
    let p5 = document.createElement("p");

    p1.appendChild(btn);
    p1.appendChild(btn2);
    p2.appendChild(span);
    p2.appendChild(input1);
    p3.appendChild(spa2);
    p3.appendChild(input2);
    p4.appendChild(btn3);
    p5.appendChild(span3);
    p5.appendChild(input3);
    infoBlock.appendChild(p2)
    infoBlock.appendChild(p3)
    infoBlock.appendChild(p5)
    infoBlock.appendChild(p1)
    infoBlock.appendChild(p4)

    let CenterX = 400,
        CenterY = 300,
        radius;

    let console1 = document.getElementById("console");
    console1.style.position = "relative";
    let n = 5;
    btn.addEventListener("click", () => {
        radius = Number(input3.value);
        n = Number(input1.value);
        for (let i = 0; i < n; i++) {
            let circle = document.createElement("span");
            circle.classList.add("circle");
            circle.id = "circle" + i;
            circle.style.position = "absolute";
            console.log(2 * Math.PI * i / n);
            circle.style.left = (CenterX + radius * Math.cos(2 * Math.PI * i / n)) + "px";
            circle.style.bottom = (CenterY + radius * Math.sin(2 * Math.PI * i / n)) + "px";
            console1.appendChild(circle);
        }
    });

    let inter;

    btn2.addEventListener("click", () => {
        let count = 0;
        inter = setInterval(() => {
            count += 1;
            for (let i = 0; i < n; i++) {
                let circle = document.getElementById("circle" + i);
                circle.style.left = (CenterX + radius * Math.cos(2 * Math.PI * i / n + count)) + "px";
                circle.style.bottom = (CenterY + radius * Math.sin(2 * Math.PI * i / n + count)) + "px";
            }
        }, input2.value);
    });

    btn3.addEventListener("click", () => {
        clearInterval(inter);
        for (let i = 0; i < n; i++) {
            let circle = document.getElementById("circle" + i);
            console1.removeChild(circle);
        }
    });
}