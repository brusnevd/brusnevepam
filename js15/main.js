let h5 = document.createElement("h5");
h5.innerText = "What to do";

let divlist = document.createElement("div");
divlist.classList.add("list");
let inputt = document.createElement("input");
inputt.classList.add("input")
let btn1 = document.createElement("button");
btn1.classList.add("btnadd");
btn1.innerText = "Add";
let btn2 = document.createElement("button");
btn2.classList.add("btnrem");
btn2.innerText = "Remove selected";
let btn3 = document.createElement("button");
btn3.classList.add("btnhid");
btn3.innerText = "Hide selected";
let btn4 = document.createElement("button");
btn4.classList.add("btnshw");
btn4.innerText = "Show hidden";

document.body.appendChild(h5);
document.body.appendChild(divlist);
document.body.appendChild(inputt);
document.body.appendChild(btn1);
document.body.appendChild(btn2);
document.body.appendChild(btn3);
document.body.appendChild(btn4);

let count = 0;

let btnadd = document.querySelector(".btnadd"),
    input = document.querySelector(".input"),
    list = document.querySelector(".list"),
    btnrem = document.querySelector(".btnrem"),
    btnhid = document.querySelector(".btnhid"),
    btnshw = document.querySelector(".btnshw");

btnadd.addEventListener("click", function() {
    let p = document.createElement("p"),
        label = document.createElement("label"),
        checkbox = document.createElement("input");

    checkbox.type = "checkbox";
    checkbox.id = "checkbox" + count;
    label.innerText = input.value;
    label.htmlFor = checkbox.id;

    p.appendChild(checkbox);
    p.appendChild(label);
    p.classList.add("task");

    list.appendChild(p);
    count += 1;
});

btnrem.addEventListener("click", function() {
    let tasks = Array.from(document.querySelectorAll(".task"));

    tasks.forEach((elem) => {
        if (elem.querySelector("input").checked) {
            list.removeChild(elem);
        }
    });
});

btnhid.addEventListener("click", function() {
    let tasks = Array.from(document.querySelectorAll(".task"));

    tasks.forEach((elem) => {
        if (elem.querySelector("input").checked) {
            elem.classList.add("hidden");
            elem.style.display = "none";
        }
    });
});

btnshw.addEventListener("click", function() {
    let tasks = Array.from(document.querySelectorAll(".task"));

    tasks.forEach((elem) => {
        if (elem.classList.contains("hidden")) {
            elem.classList.remove("hidden");
            elem.style.display = "block";
        }
    });
});