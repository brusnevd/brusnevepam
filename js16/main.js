const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

// Рисунок 1 - лицо

ctx.lineWidth = 3;
ctx.strokeStyle = '#1D4F59';

// Голова
ctx.fillStyle = '#90CAD7';
ctx.beginPath();
ctx.ellipse(295, 280, 90, 80, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.stroke();

// глаз 1
ctx.fillStyle = '#90CAD7';
ctx.beginPath();
ctx.ellipse(250, 260, 14, 10, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.stroke();

// зрачок 1
ctx.fillStyle = '#22545F';
ctx.beginPath();
ctx.ellipse(245, 260, 4, 8, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.stroke();

// глаз 2
ctx.fillStyle = '#90CAD7';
ctx.beginPath();
ctx.ellipse(305, 260, 14, 10, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.stroke();

// зрачок 2
ctx.fillStyle = '#22545F';
ctx.beginPath();
ctx.ellipse(300, 260, 4, 8, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.stroke();

// нос
ctx.beginPath();
ctx.moveTo(275, 275);
ctx.lineTo(270, 305);
ctx.lineTo(280, 305);
ctx.stroke();

// рот
ctx.fillStyle = '#90CAD7';
ctx.beginPath();
ctx.ellipse(280, 330, 30, 10, Math.PI * .03, 0, 2 * Math.PI);
ctx.fill();
ctx.stroke();

ctx.strokeStyle = '#000000';

// нижняя поверхность шляпы
ctx.fillStyle = '#396693';
ctx.beginPath();
ctx.ellipse(290, 210, 100, 20, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.stroke();

// нижняя часть цилиндра
ctx.fillStyle = '#396693';
ctx.beginPath();
ctx.ellipse(300, 200, 50, 20, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.stroke();

// квадрат цилиндра
ctx.fillStyle = "#396693";
ctx.fillRect(250, 100, 100, 100);

// верхняя часть цилиндра
ctx.fillStyle = '#396693';
ctx.beginPath();
ctx.ellipse(300, 100, 50, 20, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.stroke();

// боковые линии цилиндра
ctx.beginPath();
ctx.moveTo(250, 100);
ctx.lineTo(250, 200);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(350, 100);
ctx.lineTo(350, 200);
ctx.stroke();



// рисунок 2 - велосипед

// колёса
ctx.fillStyle = '#90CAD7';
ctx.beginPath();
ctx.ellipse(300, 700, 90, 90, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.strokeStyle = '#337D8F';
ctx.lineWidth = 5;
ctx.stroke();

ctx.fillStyle = '#90CAD7';
ctx.beginPath();
ctx.ellipse(670, 700, 90, 90, 0, 0, 2 * Math.PI);
ctx.fill();
ctx.strokeStyle = '#337D8F';
ctx.stroke();

// линии велосипеда
ctx.beginPath();
ctx.moveTo(300, 700);
ctx.lineTo(400, 580);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(300, 700);
ctx.lineTo(450, 700);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(450, 700);
ctx.lineTo(375, 520);
ctx.stroke();

// сидение
ctx.beginPath();
ctx.moveTo(350, 520);
ctx.lineTo(400, 520);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(400, 580);
ctx.lineTo(650, 580);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(450, 700);
ctx.lineTo(650, 580);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(670, 700);
ctx.lineTo(640, 500);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(433, 683);
ctx.lineTo(410, 660);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(470, 713);
ctx.lineTo(493, 730);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(640, 500);
ctx.lineTo(570, 520);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(640, 500);
ctx.lineTo(680, 450);
ctx.stroke();

// цепь где висит
ctx.beginPath();
ctx.ellipse(450, 700, 25, 25, 0, 0, 2 * Math.PI);
ctx.strokeStyle = '#337D8F';
ctx.stroke();


// рисунок 3 - дом
ctx.beginPath();
ctx.strokeStyle = '#000000';
ctx.lineWidth = 2.5;

// Квдрат дома
ctx.fillStyle = "#975B5B";
ctx.fillRect(300, 1300, 400, 400);
ctx.fill();

ctx.fillStyle = "#975B5B";
ctx.strokeRect(300, 1300, 400, 400);
ctx.stroke();

// окна
ctx.fillStyle = "#000000";
ctx.fillRect(320, 1320, 70, 50);
ctx.stroke();
ctx.fillStyle = "#000000";
ctx.fillRect(395, 1320, 70, 50);
ctx.stroke();
ctx.fillStyle = "#000000";
ctx.fillRect(320, 1375, 70, 50);
ctx.stroke();
ctx.fillStyle = "#000000";
ctx.fillRect(395, 1375, 70, 50);
ctx.stroke();

ctx.fillStyle = "#000000";
ctx.fillRect(530, 1320, 70, 50);
ctx.stroke();
ctx.fillStyle = "#000000";
ctx.fillRect(605, 1320, 70, 50);
ctx.stroke();
ctx.fillStyle = "#000000";
ctx.fillRect(530, 1375, 70, 50);
ctx.stroke();
ctx.fillStyle = "#000000";
ctx.fillRect(605, 1375, 70, 50);
ctx.stroke();

ctx.fillStyle = "#000000";
ctx.fillRect(530, 1520, 70, 50);
ctx.stroke();
ctx.fillStyle = "#000000";
ctx.fillRect(605, 1520, 70, 50);
ctx.stroke();
ctx.fillStyle = "#000000";
ctx.fillRect(530, 1575, 70, 50);
ctx.stroke();
ctx.fillStyle = "#000000";
ctx.fillRect(605, 1575, 70, 50);
ctx.stroke();

// двери
ctx.beginPath();
ctx.ellipse(395, 1520, 70, 35, 0, 0, 2 * Math.PI);
ctx.stroke();

ctx.fillStyle = "#975B5B";
ctx.fillRect(325, 1520, 140, 180);

ctx.beginPath();
ctx.fillStyle = '#975B5B';
ctx.moveTo(325, 1520); // первая внешняя точка
ctx.lineTo(325, 1700); //вторая внешняя точка
ctx.stroke();

ctx.moveTo(465, 1520); // первая внешняя точка
ctx.lineTo(465, 1700); //вторая внешняя точка
ctx.stroke();

ctx.moveTo(395, 1485); // первая внешняя точка
ctx.lineTo(395, 1700); //вторая внешняя точка
ctx.stroke();

ctx.beginPath();
ctx.ellipse(385, 1600, 5, 5, 0, 0, 2 * Math.PI);
ctx.stroke();

ctx.beginPath();
ctx.ellipse(405, 1600, 5, 5, 0, 0, 2 * Math.PI);
ctx.stroke();

// Крыша
ctx.beginPath();
ctx.fillStyle = '#975B5B';
ctx.moveTo(500, 1050); // первая внешняя точка
ctx.lineTo(700, 1300); //вторая внешняя точка
ctx.lineTo(300, 1300); //третья внешняя точка
ctx.lineTo(500, 1050); //третья внешняя точка
ctx.fill();
ctx.stroke();

ctx.fillStyle = "#975B5B";
ctx.fillRect(600, 1150, 40, 100);
ctx.strokeRect(600, 1150, 40, 100);

ctx.fillRect(602, 1248, 37, 4);

ctx.beginPath();
ctx.lineWidth = 5;
ctx.ellipse(620, 1150, 20, 5, 0, 0, 2 * Math.PI);
ctx.stroke();
ctx.fill();