document.querySelector("p button").addEventListener("click", () => {
    doing();
})

function doing() {
    const n = document.querySelector(".n").value;

    let ingredientsBlock = document.querySelector(".ingredients"),
        boiler = document.querySelector(".boiler img");

    function randomInteger(min, max) {
        // случайное число от min до (max+1)
        let rand = min + Math.random() * (max + 1 - min);
        return Math.floor(rand);
    }

    for (let i = 0; i < n; i++) {
        console.log(i % n + 1);
        let ing = document.createElement("img");
        ing.src = `ing${i % 18 + 1}.png`;
        ing.classList.add("ing");
        ing.style.left = randomInteger(200, document.documentElement.clientWidth - 200) + "px";
        ing.style.top = randomInteger(100, document.documentElement.clientHeight - 600) + "px";
        ingredientsBlock.appendChild(ing);

        let currentDroppable = null;

        let ball = ing;
        let flag = 0;

        ball.onmousedown = function(event) {

            let shiftX = event.clientX - ball.getBoundingClientRect().left;
            let shiftY = event.clientY - ball.getBoundingClientRect().top;

            ball.style.position = 'absolute';
            ball.style.zIndex = 1000;
            document.body.append(ball);

            moveAt(event.pageX, event.pageY);

            function moveAt(pageX, pageY) {
                ball.style.left = pageX - shiftX + 'px';
                ball.style.top = pageY - shiftY + 'px';
            }

            function onMouseMove(event) {
                moveAt(event.pageX, event.pageY);

                ball.hidden = true;
                let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
                ball.hidden = false;

                if (!elemBelow) return;

                let droppableBelow = elemBelow.closest('.droppable');
                if (currentDroppable != droppableBelow) {
                    if (currentDroppable) { // null если мы были не над droppable до этого события
                        // (например, над пустым пространством)
                        leaveDroppable(currentDroppable);
                        flag = 0;
                    }
                    currentDroppable = droppableBelow;
                    if (currentDroppable) { // null если мы не над droppable сейчас, во время этого события
                        // (например, только что покинули droppable)
                        enterDroppable(currentDroppable);
                        flag = 1;
                    }
                }
            }

            document.addEventListener('mousemove', onMouseMove);

            ball.onmouseup = function() {
                if (flag == 1) {
                    ball.style.display = "none";
                    boiler.src = "afteraddboiler.png";
                    setTimeout(() => {
                        boiler.src = "boiler.png";
                    }, 400);
                }
                document.removeEventListener('mousemove', onMouseMove);
                ball.onmouseup = null;
            };

        };

        function enterDroppable(elem) {
            boiler.src = "activeboiler.png";
        }

        function leaveDroppable(elem) {
            elem.style.background = '';
            boiler.src = "boiler.png";
        }

        ball.ondragstart = function() {
            return false;
        };
    }
}