function stringFormat() {
    let str = arguments[0];
    let reg = new RegExp("{[^}]*}", "ig");
    let res = Array.from(str.match(reg));
    res.forEach(element => {
        str = str.replace(element, arguments[Number(element.slice(1, element.length - 1)) + 1]);
    });
    return str;
}

function generateHtmlList(obj, template) {
    let reg = new RegExp("{[^}]*}", "ig");
    let res = Array.from(template.match(reg));
    let newHtml = document.createElement("ul");
    obj.forEach(element => {
        let li = document.createElement("li");
        let copy = template.slice();
        res.forEach((elem) => {
            copy = copy.replace(elem, element[elem.slice(1, elem.length - 1)]);
        });
        li.innerHTML = copy;
        newHtml.appendChild(li);
    });
    return newHtml.innerHTML;
}