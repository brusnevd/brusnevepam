let infoBlock = document.getElementById("task-info"),
    taskText = "";

jsConsole.writeLine("Интересные задания, понравилось их решать!", "h2");

for (let item of document.getElementsByClassName("item")) {
    item.addEventListener("click", (event) => {
        for (let item1 of document.getElementsByClassName("item")) {
            item1.classList.remove("active");
        }
        item.classList.add("active");
        jsConsole.clear();
        switch (item.innerText) {
            case "Task 1":
                taskText = 'Напишите функцию, которая форматирует строку с помощью заполнителей';
                infoBlock.innerText = taskText;
                Task1();
                break;
            case "Task 2":
                taskText = `Написать функцию, которая создает HTML UL, используя шаблон для каждого HTML LI. Источник списка должен быть массивом элементов. Замените все заполнители, помечен-ные - {... } - ' значением соответствующего свойства объекта. `;
                infoBlock.innerText = taskText;
                Task2();
                break;
        }
    });
}

function Task1() {
    let input1 = document.createElement("input");
    let span1 = document.createElement("span");
    let span2 = document.createElement("span");
    span1.innerText = "Text - ";
    span2.innerText = 'Аргументы функции - ("Hello, my name is {0}, surname is {3}, Отчество is {2}, мне {5} лет. Язык программирования {4} очень гибкий. {1}", "Дима", "Пока.", "Андреевич", "Бруснев", "JavaScript", 19)';

    let btn = document.createElement("button");
    btn.innerText = "Parse text";
    let p1 = document.createElement("p");
    let p2 = document.createElement("p");

    // p1.appendChild(span1);
    // p1.appendChild(input1);
    p2.appendChild(span2);

    infoBlock.appendChild(p1);
    infoBlock.appendChild(p2);

    infoBlock.appendChild(btn);

    btn.addEventListener("click", () => {
        let answer = stringFormat("Hello, my name is {0}, surname is {3}, Отчество is {2}, мне {5} лет. Язык программирования {4} очень гибкий. {1}", "Дима", "Пока.", "Андреевич", "Бруснев", "JavaScript", 19);

        jsConsole.writeLine(answer);
    });
}

function Task2() {
    let input1 = document.createElement("input");
    let span1 = document.createElement("span");
    span1.innerText = "Text  - ";
    let span2 = document.createElement("span");
    span2.innerText = `Текст вызова функции - generateHtmlList([
        { name: "Шапокляк", age: 55 },
        { name: "Чебурашка", age: 17 },
        { name: "Крыска-Лариска", age: 18 },
        { name: "Крокодильчик", age: 26 },
        { name: "Турист- завтрак крокодильчика", age: 32 },
    ], "<strong>-{name}-</strong> <span>-{age}-</span>");`;

    let btn = document.createElement("button");
    btn.innerText = "Parse";
    let p1 = document.createElement("p");
    let p2 = document.createElement("p");

    // p1.appendChild(span1);
    // p1.appendChild(input1);
    p2.appendChild(span2);

    infoBlock.appendChild(p1);
    infoBlock.appendChild(p2);

    infoBlock.appendChild(btn);

    btn.addEventListener("click", () => {
        let answer = generateHtmlList([
            { name: "Шапокляк", age: 55 },
            { name: "Чебурашка", age: 17 },
            { name: "Крыска-Лариска", age: 18 },
            { name: "Крокодильчик", age: 26 },
            { name: "Турист- завтрак крокодильчика", age: 32 },
        ], "<strong>-{name}-</strong> <span>-{age}-</span>");

        jsConsole.writeLine(answer, "div");
    });
}