function randomInteger(min, max) {
    // получить случайное число от (min-0.5) до (max+0.5)
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}

function getRandomInitNum() {
    let nums = "1234567890";
    let randNum = nums[randomInteger(0, 8)];
    nums = nums.replace(randNum, "");
    for (let i = 0; i < 3; i++) {
        randNum += nums[randomInteger(0, 9 - randNum.length)];
        nums = nums.replace(randNum[randNum.length - 1], "");
    }
    return randNum;
}

function guessHandler(event) {
    let inputNum = document.querySelector(".num").value;
    let commentString = document.querySelector(".comment");
    if (inputNum.length != 4) {
        commentString.innerText = "Введите четырёхзначное число";
        return;
    } else if (inputNum[0] == "0") {
        commentString.innerText = "Первая цифра не может быть 0";
        return;
    }
    let sheeps = 0,
        rams = 0;
    for (let i = 0; i < 4; i++) {
        for (let j = 0; j < 4; j++) {
            if (inputNum[i] == INITNUM[j]) {
                if (i == j) {
                    rams += 1;
                } else {
                    sheeps += 1;
                }
            }
        }
    }
    commentString.innerText = `Sheeps - ${sheeps}, Rams - ${rams}`;
    document.querySelector(".moves").innerText = Number(document.querySelector(".moves").innerText) + 1;
    if (rams == 4) {
        commentString.innerText = "Поздравляю, Вы победили. Введите Ваше имя";
        let nickBlock = document.querySelector(".nick");
        nickBlock.style.display = "inline";
        document.querySelector(".saveres").style.display = "inline";
        document.querySelector(".saveres").addEventListener("click", () => {
            if (localStorage.getItem("dbresults")) {
                let results = JSON.parse(localStorage.getItem("dbresults"));
                results.push({
                    name: nickBlock.value,
                    moves: document.querySelector(".moves").innerText
                });
                localStorage.setItem("dbresults", JSON.stringify(results));
            } else {
                localStorage.setItem("dbresults", JSON.stringify([{
                    name: nickBlock.value,
                    moves: document.querySelector(".moves").innerText
                }]));
            }
            document.querySelector(".results").style.display = "block";
            let results = JSON.parse(localStorage.getItem("dbresults"));
            results.sort((a, b) => a.moves - b.moves);
            results.forEach(element => {
                let p = document.createElement("p");
                p.innerText = element.name + ", попыток - " + element.moves;
                document.querySelector(".results").appendChild(p);
            });
        })
    }
}

const INITNUM = getRandomInitNum();

let btnguess = document.querySelector(".guess");
btnguess.addEventListener("click", guessHandler);

let btnans = document.querySelector(".btnanswer");
btnans.addEventListener("click", () => {
    let commentString = document.querySelector(".comment");
    commentString.innerText = "Ответ - " + INITNUM;
});